Drop table if exists Chat;

create table Chat (
  Id integer primary key autoincrement,
  Message text not null,
  Username text not null
);
