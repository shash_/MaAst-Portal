from flask import Flask, session, render_template, redirect, url_for, request, flash
from functionfile import *
from myfuncs import *
from chatmod import *

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def init():
    print("render init.html")
    return render_template('gravity.html',template_folder = "templates")

@app.route('/signup', methods=['GET','POST'])
def signup():
    derror = None
    if request.method == 'POST':
        username = request.form['username']
        name = request.form['name']
        password = request.form['password']
        email = request.form['email']
        check = request.form['password2']
        if password != check:
            derror = "Passwords Do Not Match!!!"
            return render_template('login.html',display_error=derror,template_folder='templates')
        if uexists(username):
            derror = "Username Already Taken!!!"
            return render_template('login.html',display_error=derror,template_folder='templates')
        if eexists(email) != False:
            derror = "Email Already Exists!!!"
            return render_template('login.html',display_error=derror,template_folder='templates')

        hpass = pinit(password)
        adduser(username,name,hpass,email)
        derror = "User Created!!! Please LogIn To Continue..."
    return render_template('login.html',display_error=derror,template_folder='templates')


@app.route('/login', methods=['GET','POST'])
def login():
    derror = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        success = verify(username,password)
        if not success:
            derror = "The Username and Password don't seem to match!!! Try once more!!!"
            return render_template('login.html',display_error=derror,template_folder='templates')
        session['username'] = username
        return redirect(url_for('meraprofile'))
    else:
        derror = "Means of Logging in not identified!!! If this is a bug, Please report to us at <a href='/reportbug.html'> This Page </a>"
        derror = ""
        return render_template('login.html',display_error=derror,template_folder='templates')

@app.route('/myprofile')
def meraprofile():
    if 'username' in session :
        username=session['username']
        userdata = getuser(username)
        return render_template('myprofile.html',template_folder='templates',userdata=userdata)
        #return redirect(url_for('solar'))
    return "The Page You Are Looking For Can Only Be Accessed By Logging In!!!"

@app.route('/solar')
def solar():
    return render_template('solar.html', template_folder='templates')

@app.route('/reportbug',methods=['Get','POST'])
def reportbug():
    getbug()

@app.route('/logout')
def logout():
    if 'username' in session:
        session.pop('username',None)
        return redirect(url_for('init'))
    else:
        return redirect(url_for('meraprofile'))


app.secret_key = "AreyBhaiBhaiBhai"

if __name__ == '__main__':
    app.run(debug=True)
