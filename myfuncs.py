import hashlib
from passlib.hash import pbkdf2_sha256

def hashit(password):
	pass_enc = password.encode()
	hpass_obj = hashlib.sha512(pass_enc)
	hpass = hpass_obj.hexdigest()
	return hpass
def pinit(pin):
	#pin_enc = pin.encode()
	#hpin_obj = hashlib.sha256(pin_enc)
	#hpin = 
	hpin = pbkdf2_sha256.encrypt(pin, rounds = 10, salt_size = 2 ) #Have a look here later!
	return hpin