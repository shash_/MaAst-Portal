drop table if exists UserProfile;
create table UserProfile
(
	Userid Integer primary key,
	Username Text not null,
	Name text not null,
	Password text not null,
	Email Text not null,
	Upvotes integer not null,
	Downvotes integer not null
);
