from chatmod import *

class MakeTable:
	def __init__(self,type,name):
		self.type = type
		self.name = name

	def create():
		x=tabexists(self.name)
		if x:
			return False
		try:
			tabcreate(self.type , self.name)
		except :
			return False
		return True

	def delete():
		drop(self.name)
		return tabexists(self.name)
