from flask_socketio import *
from flask import *
import sqlite3 as sql
import wikipedia as wiki
import re

app = Flask(__name__)
app.config['SECRET KEY'] = " pwrogrw[ocmewiont]"
socketio  = SocketIO(app)

@app.route('/')
def home():
    db = sql.connect("chat.db")
    cur = db.cursor()
    a = cur.execute("select message from chat")
    messages = a.fetchall()
    for i in range(len(messages)):
        messages[i] = str(messages[i]).replace("(u'", "")
        messages[i] = str(messages[i]).replace("',)", "")
        messages[i] = str(messages[i]).strip()
    db.commit()
    db.close()
    return render_template("chat.html", messages=messages, template_folder="templates")
@socketio.on('message')
def handlemessage(msg):
    print("message received")
    print("msg: " + msg)
    db = sql.connect("chat.db")
    cur = db.cursor()
    cur.execute("INSERT INTO chat (message) values(?)",(msg,))
    db.commit()
    db.close()
    send(msg, broadcast=True)

@app.route('/search/', methods=['POST', 'GET'])
def find_query():
    if request.method == "POST":
        query = request.form['query']
        print(query+"\n")
        try:
            summ = wiki.summary(query)
            summ = summ.replace('\n','')
            summ = " ".join(summ.split())
            print(summ)
            for i in re.findall('\{\\\displaystyle .*\}', summ):
                summ = summ.replace(i,'$$'+i+'$$')
                print(summ)
        except:
            summ = "Sorry but apparently this doesn't exist :(  Or maybe our program is bad"
    return render_template('content.html', heading=query, summ=summ, template_folder="templates")

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=8000, debug=True)
